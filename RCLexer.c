#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RCLexer.h"



// == StringStorage == //
#define STRINGSTORAGE_LINK_SIZE 512
#define SS_Link_Space(link) (STRINGSTORAGE_LINK_SIZE - (link)->count)
struct SS_Link
{
   char base[STRINGSTORAGE_LINK_SIZE];
   size_t count;
   struct SS_Link * next;
};

struct StringStorage
{
   struct SS_Link * root;
};

static void StringStorage_Init(struct StringStorage * ss)
{
   ss->root = NULL;
}

static void StringStorage_Free(struct StringStorage * ss)
{
   struct SS_Link * tLink, * dLink;
   
   tLink = ss->root;
   while(tLink != NULL)
   {
      dLink = tLink;
      tLink = tLink->next;
      free(dLink);
   }
   ss->root = NULL;
}

static const char * StringStorage_GetSized(struct StringStorage * ss, const char * str, size_t size)
{
   struct SS_Link * link, *tLink;
   size_t storageSize = size + 1;
   char * ptr = NULL;
   // Check for existing string
   
   tLink = ss->root;
   while(tLink != NULL)
   {
      size_t index = 0;
      while(index < tLink->count)
      {
         const char * sss = &tLink->base[index];
         size_t lSize = strlen(sss);         
         if(lSize == size && memcmp(sss, str, size) == 0)
         {
            return sss;
         }
         index += lSize + 1;
      }
      
      
      tLink = tLink->next;
   }
   // If we get here, we didn't find a match
   
   // Adding to New Link
   if(ss->root = NULL)
   {
      link = malloc(sizeof(struct SS_Link));
      link->next = NULL;
      link->count = 0;
      
      ss->root = link;
      
   }
   else
   {
      link = NULL;
      tLink = ss->root;
      while(tLink->next != NULL && link == NULL)
      {
         if(SS_Link_Space(tLink) >= storageSize)
         {
            link = tLink;
         }
         tLink = tLink->next;
      }
      if(link == NULL && SS_Link_Space(tLink) >= storageSize)
      {
         link = tLink;
      }
      
      if(link == NULL)
      {
         link = malloc(sizeof(struct SS_Link));
         link->next = NULL;
         link->count = 0;
         
         tLink->next = link;
         
      }
      
   }
   
   ptr = &link->base[link->count];
   memcpy(ptr, str, size);
   
   ptr[size] = '\0';
   link->count += storageSize;
   return ptr;
}

static const char * StringStorage_GetNullTermed(struct StringStorage * ss, const char * str)
{
   return StringStorage_GetSized(ss, str, strlen(str));
}

#define INPUTSYS_BUFFER_SIZE 512
struct InputSys
{
   char buffer[INPUTSYS_BUFFER_SIZE];
   size_t bufferIndex;
   size_t bufferFilledAmount;
   FILE * fh;
   int ownsFilehandle;
   int col;
   int line;
   const char * filename;
};

static void InputSys_InitFromFileHandle(struct InputSys * is, FILE * fh, const char * filename)
{
   is->bufferIndex = 0;
   is->bufferFilledAmount = 0;
   is->fh = fh;
   is->col = 1;
   is->line = 1;
   is->filename = filename;
   is->ownsFilehandle = 0;
}

static void InputSys_InitFromFilename(struct InputSys * is, const char * filename)
{
   FILE *fh = fopen(filename, "r");
   
   if(fh != NULL)
   {
      InputSys_InitFromFileHandle(is, fh, filename);
   }
   is->ownsFilehandle = 1;
}

static void InputSys_Free(struct InputSys * is)
{
   if(is->fh != NULL && is->ownsFilehandle)
   {
      fclose(is->fh);
      is->fh = NULL;
   }
}

static void InputSys_CycleBuffer(struct InputSys * is)
{
   is->bufferIndex = 0;
   if(is->fh != NULL)
   {
      is->bufferFilledAmount = fread(is->buffer, 1, INPUTSYS_BUFFER_SIZE, is->fh);
   }
   else
   {
      is->bufferFilledAmount = 0;
   }
}

static int InputSys_Peek(struct InputSys * is, const char ** filename, int * line, int * col)
{
   if(is->bufferIndex >= is->bufferFilledAmount)
   {
      InputSys_CycleBuffer(is);
   }
   if(is->bufferIndex >= is->bufferFilledAmount)
   {
      return -1;
   }
   if(filename != NULL)
   {
      (*filename) = is->filename;
   }
   if(line != NULL)
   {
      (*line) = is->line;
   }
   if(col != NULL)
   {
      (*col) = is->col;
   }
   return is->buffer[is->bufferIndex];
}


static int InputSys_Next(struct InputSys * is, const char ** filename, int * line, int * col)
{
   int c;
   
   c = InputSys_Peek(is, filename, line, col);
   
   is->col ++;
   if(c == '\n')
   {
      is->col = 1;
      is->line ++;
   }
   
   is->bufferIndex ++;
   
   return c;
}


struct StringBuffer
{
   size_t size;
   size_t count;
   char * base;
};

static void StringBuffer_Init(struct StringBuffer * buf)
{
   buf->size = 0;
   buf->count = 0;
   buf->base = NULL;
}

static void StringBuffer_Free(struct StringBuffer * buf)
{
   if(buf->base != NULL)
   {
      free(buf->base);
      buf->base = NULL;
   }
   buf->size = 0;
   buf->count = 0;
}

static void StringBuffer_Add(struct StringBuffer * buf, int c)
{
   if(buf->count >= buf->size)
   {
      buf->size = buf->count + 32;
      buf->base = realloc(buf->base, buf->size);
   }
   
   buf->base[buf->count] = c;
   buf->count ++;
}

static void StringBuffer_Reset(struct StringBuffer * buf)
{
   buf->count = 0;
}

static const char * StringBuffer_Get(struct StringBuffer * buf, struct StringStorage * ss)
{
   return StringStorage_GetSized(ss, buf->base, buf->count);
}

static int StringBuffer_Match(struct StringBuffer * buf, const char * str)
{
   size_t strSize = strlen(str);
   return strSize == buf->count && memcmp(buf->base, str, buf->count) == 0;
}

static int StringBuffer_GreedyMatch(struct StringBuffer * buf, int c, 
                                    const char * str)
{
   size_t strSize = strlen(str);
   if(strSize <= buf->count)
   {
      return 0;
   }
   return memcpy(buf->base, str, buf->count) == 0 && c == str[buf->count];
}

static int StringBuffer_IsEmpty(struct StringBuffer * buf)
{
   return buf->count == 0;
}

static int StringBuffer_LastChar(struct StringBuffer * buf)
{
   if(buf->count <= 0) return -1;
   return buf->base[buf->count - 1];
}


struct RCLTokenPair
{
   enum RCLTokenType type;
   const char * text;
};

static enum RCLTokenType RCLTokenPair_Find(struct RCLTokenPair * pairs, 
                                           struct StringBuffer * buf)
{
   enum RCLTokenType found = eRCLTT_Unknown;
   int i = 0;
   while(pairs[i].text != NULL)
   {
      if(StringBuffer_Match(buf, pairs[i].text))
      {
         found = pairs[i].type;
         break;
      }
   }
   return found;
}

static enum RCLTokenType RCLTokenPair_GreedyFind(struct RCLTokenPair * pairs,
                                                 struct StringBuffer * buf,
                                                 int c)
{
   enum RCLTokenType found = eRCLTT_Unknown;
   int i = 0;
   while(pairs[i].text != NULL)
   {
      if(StringBuffer_GreedyMatch(buf, c, pairs[i].text))
      {
         found = pairs[i].type;
         break;
      }
   }
   return found;
}


static struct RCLTokenPair RCLTextTokenList[] = {

   { eRCLTT_Struct,    "struct"   },
   { eRCLTT_Enum,      "enum"     },
   { eRCLTT_Union,     "union"    },
   { eRCLTT_Typedef,   "typedef"  },
   { eRCLTT_If,        "if"       },
   { eRCLTT_Else,      "else"     },
   { eRCLTT_For,       "for"      },
   { eRCLTT_Do,        "do"       },
   { eRCLTT_While,     "while"    },
   { eRCLTT_Goto,      "goto"     },
   { eRCLTT_Switch,    "switch"   },
   { eRCLTT_Case,      "case"     },
   { eRCLTT_Break,     "break"    },
   { eRCLTT_Default,   "default"  },
   { eRCLTT_Static,    "static"   },
   { eRCLTT_Extern,    "extern"   },
   { eRCLTT_Inline,    "inline"   },
   { eRCLTT_Volatile,  "volatile" },
   { eRCLTT_Void,      "void"     },
   { eRCLTT_Const,     "const"    },
   { eRCLTT_Return,    "return"   },
   { eRCLTT_Auto,      "auto"     },
   { eRCLTT_Unknown,   NULL       }
};


static struct RCLTokenPair RCLPunctuatorTokenList[] = {
   { eRCLTT_Plus,                  "+"  },
   { eRCLTT_Minus,                 "-"  },
   { eRCLTT_Asterisk,              "*"  },
   { eRCLTT_Divide,                "/"  },
   { eRCLTT_PlusEquals,            "+=" },
   { eRCLTT_MinusEquals,           "-=" },
   { eRCLTT_DivideEquals,          "/=" },
   { eRCLTT_MultiplyEquals,        "*=" },
   { eRCLTT_Increment,             "++" },
   { eRCLTT_Decrement,             "--" },
   { eRCLTT_Comma,                 ","  },
   { eRCLTT_Dot,                   "."  },
   { eRCLTT_DereferenceDot,        "->" },
   { eRCLTT_Colon,                 ":"  },
   { eRCLTT_OpenParen,             "("  },
   { eRCLTT_ClosedParen,           ")"  },
   { eRCLTT_OpenSquareBraket,      "["  },
   { eRCLTT_ClosedSquareBraket,    "]"  },
   { eRCLTT_OpenCurlyBracket,      "{"  },
   { eRCLTT_ClosedCurlyBracket,    "}"  },
   { eRCLTT_Semicolon,             ";"  },
   { eRCLTT_Equals,                "="  },
   { eRCLTT_DoubleEquals,          "==" },
   { eRCLTT_LessThan,              "<"  },
   { eRCLTT_GreaterThan,           ">"  },
   { eRCLTT_LessThanOrEqualTo,     "<=" },
   { eRCLTT_GreaterThanOrEqualTo,  ">=" },
   { eRCLTT_NotEqual,              "!=" },
   { eRCLTT_BitShiftLeft,          "<<" },
   { eRCLTT_BitShiftRight,         ">>" },
   { eRCLTT_BitwiseNot,            "~"  },
   { eRCLTT_BitwiseAnd,            "&"  },
   { eRCLTT_BitwiseOr,             "|"  },
   { eRCLTT_LogicalNot,            "!"  },
   { eRCLTT_LogicalAnd,            "&&" },
   { eRCLTT_LogicalOr,             "||" },
   { eRCLTT_Unknown,               NULL }
};


enum RCLStateCode
{
   eRCLSC_Continue,
   eRCLSC_TokenReady,
   eRCLSC_BlockedByInput
};
typedef enum RCLStateCode (*RCLexerStateFunction)(struct RCLexer * lexer, int c);

static enum RCLStateCode RCLexerState_Default(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_IgnoreWhitespace(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Comment(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Zero(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Integer(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Punctuator(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Text(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_String(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Character(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Fraction(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Dot(struct RCLexer * lexer, int c);

struct RCLexer
{
   struct StringStorage ss;
   struct InputSys is;
   struct StringBuffer buf;
   RCLexerStateFunction state;
   struct RCLToken token;
   int preprocessorFlag;
};

struct RCLexer * RCLexer_MakeFromFilename(const char * filename)
{
   struct RCLexer * lexer;
   lexer = malloc(sizeof(struct RCLexer));
   StringStorage_Init(&lexer->ss);
   StringBuffer_Init(&lexer->buf);
   InputSys_InitFromFilename(&lexer->is, filename);
   lexer->state = RCLexerState_Default;
   lexer->preprocessorFlag = 0;
   return lexer;
}

void RCLexer_Free(struct RCLexer * lexer)
{
   InputSys_Free(&lexer->is);
   StringBuffer_Free(&lexer->buf);
   StringStorage_Free(&lexer->ss);
   lexer->state = NULL;
}

static enum RCLStateCode RCLexer_RunState(struct RCLexer * lexer)
{
   int c; 
   c = InputSys_Peek(&lexer->is, NULL, NULL, NULL);
   if(c < 0)
   {
      return eRCLSC_BlockedByInput;
   }


   return lexer->state(lexer, c);
}

static void RCLexer_AddToBuf(struct RCLexer * lexer)
{
   int c;
   if(StringBuffer_IsEmpty(&lexer->buf))
   {
      c = InputSys_Next(&lexer->is, &lexer->token.filename, 
                                    &lexer->token.line, 
                                    &lexer->token.col);
   }
   else
   {
      c = InputSys_Next(&lexer->is, NULL, NULL, NULL);
   }
   StringBuffer_Add(&lexer->buf, c);
}

static void RCLexer_ManualAddToBuf(struct RCLexer * lexer, int c)
{
   if(StringBuffer_IsEmpty(&lexer->buf))
   {
      InputSys_Peek(&lexer->is, &lexer->token.filename, 
                                &lexer->token.line, 
                                &lexer->token.col);
   }
   StringBuffer_Add(&lexer->buf, c);
}

static void RCLexer_Drop(struct RCLexer * lexer)
{
   (void)InputSys_Next(&lexer->is, NULL, NULL, NULL);
}

int RCLexer_GetTokens(struct RCLexer * lexer, struct RCLToken * tokens, int count)
{
   int tokensFilledCount = 0;
   
   while(tokensFilledCount < count)
   {
      switch(RCLexer_RunState(lexer))
      {
      case eRCLSC_TokenReady:
         lexer->token.text = StringBuffer_Get(&lexer->buf, &lexer->ss);
         lexer->token.preprocessorFlag = lexer->preprocessorFlag;
         memcpy(&tokens[tokensFilledCount], &lexer->token, sizeof(struct RCLToken));
         tokensFilledCount ++;
         StringBuffer_Reset(&lexer->buf);
         break;
      default:
         // Intentionaly Empty
         break;
      }
   }
   
   return tokensFilledCount;
}


static int IsWhitespace(int c)
{
   return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

static int IsDigit(int c)
{
   return c >= '0' && c <= '9';
}

static int IsLetter(int c)
{
   return (c >= 'a' && c <= 'z') ||
          (c >= 'A' && c <= 'Z');
}

static enum RCLStateCode RCLexerState_Default(struct RCLexer * lexer, int c)
{
   
   if(IsWhitespace(c))
   {
      lexer->state = RCLexerState_IgnoreWhitespace;
   }
   else if(c == '/')
   {
      lexer->state = RCLexerState_Comment;
   }
   else if(c == '0')
   {
      lexer->state = RCLexerState_Zero;
   }
   else if(c == '.')
   {
      lexer->state = RCLexerState_Dot;
   }
   else if(c == '"')
   {
      lexer->state = RCLexerState_String;
      RCLexer_Drop(lexer);
   }
   else if(c == '\'')
   {
      lexer->state = RCLexerState_Character;
      RCLexer_Drop(lexer);
   }
   else if(IsDigit(c))
   {
      lexer->state = RCLexerState_Integer;
   }
   else if(IsLetter(c) || c == '_')
   {
      lexer->state = RCLexerState_Text;
   }
   
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Character(struct RCLexer * lexer, int c)
{
   if(c == '\'')
   {
      int pc = StringBuffer_LastChar(&lexer->buf);
      if(pc == '\\')
      {
         RCLexer_AddToBuf(lexer);
      }
      else
      {
         RCLexer_Drop(lexer);
         lexer->token.type = eRCLTT_Character;
         lexer->state = RCLexerState_Default;
         return  eRCLSC_TokenReady;
      }
   }
   else
   {
      RCLexer_AddToBuf(lexer);
   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_String(struct RCLexer * lexer, int c)
{
   if(c == '"')
   {
      int pc = StringBuffer_LastChar(&lexer->buf);
      if(pc == '\\')
      {
         RCLexer_AddToBuf(lexer);
      }
      else
      {
         RCLexer_Drop(lexer);
         lexer->token.type = eRCLTT_String;
         lexer->state = RCLexerState_Default;
         return  eRCLSC_TokenReady;
      }
   }
   else
   {
      RCLexer_AddToBuf(lexer);
   }
   return eRCLSC_Continue;

}
static enum RCLStateCode RCLexerState_Dot(struct RCLexer * lexer, int c)
{
   if(c == '.' && StringBuffer_IsEmpty(&lexer->buf))
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(IsDigit(c))
   {
      lexer->state = RCLexerState_Fraction;
   }
   else
   {
      lexer->state = RCLexerState_Punctuator;
   }

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Text(struct RCLexer * lexer, int c)
{
   int isDig = IsDigit(c);
   if((IsLetter(c) || c == '_') && !isDig)
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(isDig && !StringBuffer_IsEmpty(&lexer->buf))
   {
      RCLexer_AddToBuf(lexer);
   }
   else
   {
      enum RCLTokenType type;
      type = RCLTokenPair_Find(RCLTextTokenList, &lexer->buf); 
      if(type == eRCLTT_Unknown) type = eRCLTT_Literal;
      lexer->token.type = type;
      return eRCLSC_TokenReady;
   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_IgnoreWhitespace(struct RCLexer * lexer, int c)
{
   if(IsWhitespace(c))
   {
      RCLexer_Drop(lexer);
   }
   else
   {
      lexer->state = RCLexerState_Default;
      StringBuffer_Reset(&lexer->buf);
   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_IgnoreUntilNextLine(struct RCLexer * lexer, int c)
{
   if(c == '\n')
   {
      lexer->state = RCLexerState_Default;
      StringBuffer_Reset(&lexer->buf);
   }
   else
   {
      RCLexer_Drop(lexer);
   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_IgnoreUntilEndOfBlockComment(struct RCLexer * lexer, int c)
{
   if(c == '*')
   {
      StringBuffer_Reset(&lexer->buf);
      RCLexer_AddToBuf(lexer);
   }
   else if(c = '/' && StringBuffer_Match(&lexer->buf, "*"))
   {
      RCLexer_Drop(lexer);
      lexer->state = RCLexerState_Default;
      StringBuffer_Reset(&lexer->buf);
   }
   else
   {
      StringBuffer_Reset(&lexer->buf);
   }
   return eRCLSC_Continue;
}


static enum RCLStateCode RCLexerState_Comment(struct RCLexer * lexer, int c)
{
   if(c == '/')
   {
      if(StringBuffer_IsEmpty(&lexer->buf))
      {
         RCLexer_AddToBuf(lexer);
      }
      else
      {
         lexer->state = RCLexerState_IgnoreUntilNextLine;
      }
   }
   else if(c == '*' && !StringBuffer_IsEmpty(&lexer->buf))
   {
      lexer->state = RCLexerState_IgnoreUntilEndOfBlockComment;
      RCLexer_Drop(lexer);
      StringBuffer_Reset(&lexer->buf);
   }
   else
   {
      // We are not a comment. So we problably are some kind of Punctuator
   }

   
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Punctuator(struct RCLexer * lexer, int c)
{
   if(RCLTokenPair_GreedyFind(RCLPunctuatorTokenList, &lexer->buf, c) != eRCLTT_Unknown)
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(StringBuffer_IsEmpty(&lexer->buf))
   {
      lexer->state = RCLexerState_Default;
   }
   else
   {
      lexer->token.type = RCLTokenPair_Find(RCLPunctuatorTokenList, &lexer->buf);
      lexer->state = RCLexerState_Default;
      return eRCLSC_TokenReady; 
   }

   return eRCLSC_Continue;
}


static int IsOctDigit(int c)
{
   return c >= '0' && c <= '7';
}

static int IsHexDigit(int c)
{
   return IsDigit(c) || 
          (c >= 'a' && c <= 'f') ||
          (c >= 'A' && c <= 'F');
}


static enum RCLStateCode RCLexerState_HexFraction(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Integer(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Hex(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Oct(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_HexFraction(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_Exponent(struct RCLexer * lexer, int c);
static enum RCLStateCode RCLexerState_IntegerSuffixUnsigned(struct RCLexer * lexer, int c);


static enum RCLStateCode RCLexerState_IntegerSuffixLong(struct RCLexer * lexer, int c)
{
   if(c == 'l' || c == 'L')
   {
      RCLexer_ManualAddToBuf(lexer, 'l');
      if(StringBuffer_Match(&lexer->buf, "ll"))
      {
         lexer->token.type = eRCLTT_NumberIntegerSuffixLongLong;
         lexer->state = RCLexerState_IntegerSuffixUnsigned;
         return eRCLSC_TokenReady;
      }
   }
   else if(StringBuffer_Match(&lexer->buf, "l"))
   {
      lexer->token.type = eRCLTT_NumberIntegerSuffixLong;
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
      return eRCLSC_TokenReady;
   }
   else if(c == 'u' || c == 'U')
   {
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
   }
   else
   {
      lexer->state = RCLexerState_Default;
   }
}

static enum RCLStateCode RCLexerState_IntegerSuffixUnsigned(struct RCLexer * lexer, int c)
{
   if(c == 'u' || c == 'U')
   {
      RCLexer_ManualAddToBuf(lexer, 'u');
      lexer->token.type = eRCLTT_NumberHexPrefix;
      lexer->state = RCLexerState_IntegerSuffixLong;
      return eRCLSC_TokenReady;
   }
   else if(c == 'l' || c == 'L')
   {
      lexer->state = RCLexerState_IntegerSuffixLong;
   }

   lexer->state = RCLexerState_Default;

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Zero(struct RCLexer * lexer, int c)
{

   if(StringBuffer_IsEmpty(&lexer->buf) && c == '0')
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(IsOctDigit(c))
   {
      lexer->token.type = eRCLTT_NumberOctPrefix;
      lexer->state = RCLexerState_Oct;
      return eRCLSC_TokenReady;
   }
   else if((c == 'x' || c == 'X') && StringBuffer_Match(&lexer->buf, "0"))
   {
      RCLexer_ManualAddToBuf(lexer, 'x');
      lexer->token.type = eRCLTT_NumberHexPrefix;
      lexer->state = RCLexerState_Hex;
      return eRCLSC_TokenReady;
   }
   else
   {
      lexer->token.type = eRCLTT_NumberInteger;
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
      return eRCLSC_TokenReady;
   }


   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Integer(struct RCLexer * lexer, int c)
{
   if(IsDigit(c))
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(IsHexDigit(c))
   {
      lexer->state = RCLexerState_Hex;
   }
   else if(c == '.' || c == 'e' || c == 'E')
   {
      lexer->state = RCLexerState_Fraction;
   }
   else if(c == 'p' || c == 'P')
   {
      lexer->state = RCLexerState_HexFraction;
   }
   else
   {
      lexer->token.type = eRCLTT_NumberInteger;
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
      return eRCLSC_TokenReady;
   }

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Hex(struct RCLexer * lexer, int c)
{
   if(IsHexDigit(c))
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(c == '.' || c == 'p' || c == 'P')
   {
      lexer->state = RCLexerState_HexFraction;
   }
   else
   {
      lexer->token.type = eRCLTT_NumberHex;
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
      return eRCLSC_TokenReady;
   }

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Fraction(struct RCLexer * lexer, int c)
{
   if(IsDigit(c) || c == '.')
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(c == 'p' || c == 'P')
   {
      lexer->state = RCLexerState_HexFraction;
   }
   else
   {
      lexer->token.type = eRCLTT_NumberFraction;
      lexer->state = RCLexerState_Exponent;
      return eRCLSC_TokenReady;
   }

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_HexFraction(struct RCLexer * lexer, int c)
{
   if(IsHexDigit(c) || c == '.')
   {
      RCLexer_AddToBuf(lexer);
   }
   else
   {
      lexer->token.type = eRCLTT_NumberHexFraction;
      lexer->state = RCLexerState_Exponent; //TODO: Add BinaryExponent State
      return eRCLSC_TokenReady;
   }

   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Oct(struct RCLexer * lexer, int c)
{
   if(IsOctDigit(c))
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(IsDigit(c))
   {
      lexer->state = RCLexerState_Integer;
   }
   else if(IsHexDigit(c))
   {
      lexer->state = RCLexerState_Hex;
   }
   else if(c == '.' || c == 'e' || c == 'E')
   {
      lexer->state = RCLexerState_Fraction;
   }
   else if(c == 'p' || c == 'P')
   {
      lexer->state = RCLexerState_HexFraction;
   }
   else
   {
      lexer->token.type = eRCLTT_NumberOct;
      lexer->state = RCLexerState_IntegerSuffixUnsigned;
      return eRCLSC_TokenReady;
   }

   return eRCLSC_Continue;
}


static enum RCLStateCode RCLexerState_FractionSuffix(struct RCLexer * lexer, int c)
{
   lexer->state = RCLexerState_Default;
   if(c == 'f' || c == 'F')
   {
      RCLexer_AddToBuf(lexer);
      lexer->token.type = eRCLTT_NumberFractionSuffixFloat;
      return eRCLSC_TokenReady;
   }
   else if(c == 'l' || c == 'L')
   {
      RCLexer_AddToBuf(lexer);
      lexer->token.type = eRCLTT_NumberFractionSuffixLongDouble;
      return eRCLSC_TokenReady;

   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_ExponentValue(struct RCLexer * lexer, int c)
{
   if(StringBuffer_IsEmpty(&lexer->buf) && (c == '+' || c == '-'))
   {
      RCLexer_AddToBuf(lexer);
   }
   else if(IsDigit(c))
   {
      RCLexer_AddToBuf(lexer);
   }
   else
   {
      lexer->state  = RCLexerState_FractionSuffix;
   }
   return eRCLSC_Continue;
}

static enum RCLStateCode RCLexerState_Exponent(struct RCLexer * lexer, int c)
{
   if(c == 'e' || c == 'E')
   {
      RCLexer_AddToBuf(lexer);
      lexer->token.type = eRCLTT_NumberExponent;
      lexer->state = RCLexerState_ExponentValue;
      return eRCLSC_TokenReady;
   }

   if(c == 'p' || c == 'P')
   {
      RCLexer_AddToBuf(lexer);
      lexer->token.type = eRCLTT_NumberBinaryExponent;
      lexer->state = RCLexerState_ExponentValue;
      return eRCLSC_TokenReady;
   }
   lexer->state  = RCLexerState_FractionSuffix;
   
   return eRCLSC_Continue;
}

