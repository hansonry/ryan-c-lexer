#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CRLexer.h"
#include "RyanPath.h"

struct CallRecord
{
   char * filename;
   char * insideFunction;
   int    insideFunctionLine;
   char * calledFunction;
   size_t count;
};

struct Data
{
   struct CallRecord * records;
   size_t count;
   size_t size;
};

static struct Data Data;

#define RECORD_GROWBY 32
static void Data_Init(void)
{
   Data.size = RECORD_GROWBY;
   Data.count = 0;
   Data.records = malloc(sizeof(struct CallRecord) * Data.size);
}

static void Data_Destroy(void)
{
   size_t i;
   for(i = 0; i < Data.count; i++)
   {
      free(Data.records[i].filename);
      free(Data.records[i].insideFunction);
      free(Data.records[i].calledFunction);
   }
   free(Data.records);
}



static void Data_AddRecord(const char * filename, 
                           char * insideFunction, int insideFunctionLine,
                           char * calledFunction)
{
   struct CallRecord * record, * foundRecord;
   size_t i;
   // Search though the records for a duplicate
   foundRecord = NULL;
   for(i = 0; i < Data.count; i++)
   {
      record = &Data.records[i];
      if(strcmp(record->filename, filename) == 0 &&
         strcmp(record->insideFunction, insideFunction) == 0 &&
         strcmp(record->calledFunction, calledFunction) == 0 &&
         record->insideFunctionLine == insideFunctionLine)
      {
         foundRecord = record;
         break;
      }
   }
   
   if(foundRecord == NULL)
   {
      if(Data.count >= Data.size)
      {
         Data.size = Data.count + RECORD_GROWBY;
         Data.records = realloc(Data.records, sizeof(struct CallRecord) * Data.size);
      }
      
      record = &Data.records[Data.count];
      record->filename = strdup(filename);
      record->insideFunction = strdup(insideFunction);
      record->insideFunctionLine = insideFunctionLine;
      record->count = 0;
      record->calledFunction = strdup(calledFunction);
      
      Data.count++;
   }
   else
   {
      record = foundRecord;
   }
   
   record->count ++;
}                              

static void TokenizeFile(const char * filename)
{
   struct CRLexer * lexer;
   struct CRLToken * tokens;
   size_t count, i;
   

   lexer = CRLexer_New(filename);
   tokens = CRLexer_GetTokens(lexer, &count);
   
   printf("Line\tCol\tType\tText\n");   
   for(i = 0; i < count; i++)
   {
      printf("%d\t%d\t%d\t\"%s\"\n", tokens[i].line, tokens[i].col, tokens[i].type, tokens[i].text);
   }
   
   CRLexer_Destroy(lexer);

}

static char * IgnoreList[] = {
   "strlen",
   "malloc",
   "realloc",
   "memcpy",
   "sizeof",
   "strdup",
   "free",
   "printf",
   "fprintf",
   "fopen",
   "fclose",
   "strcmp",
   "memcpy",
   NULL
};

static int IsOnIgoreList(const char * functionName)
{
   size_t i;
   i = 0;
   while(IgnoreList[i] != NULL)
   {
      if(strcmp(IgnoreList[i], functionName) == 0)
      {
         return 1;
      }
      i ++;
   }
   return 0;
}

static void AddFunctionCallsToData(const char * filename)
{
   struct CRLexer * lexer;
   struct CRLToken * tokens, *insideFunctionToken;
   size_t count, i;
   int inTypedef = 0;
   int bracketDepth = 0;
   
   lexer = CRLexer_New(filename);
   tokens = CRLexer_GetTokens(lexer, &count);
   
   insideFunctionToken = NULL;
   for(i = 0; i < count; i++)
   {
      switch(tokens[i].type)
      {
      case eCRLTT_Typedef:
         inTypedef = 1;
         break;
      case eCRLTT_Semicolon:
         inTypedef = 0;
         break;
      case eCRLTT_OpenParen:
         if(!inTypedef)
         {
            if(tokens[i - 1].type == eCRLTT_Literal)
            {
               if(bracketDepth == 0)
               {
                  insideFunctionToken = &tokens[i - 1];
               }
               else if(insideFunctionToken != NULL && 
                       !IsOnIgoreList(tokens[i - 1].text))
               {
                  //printf("Line: %d\n", insideFunctionToken->line);
                  Data_AddRecord(filename, 
                                 insideFunctionToken->text, 
                                 insideFunctionToken->line, 
                                 tokens[i - 1].text);
               }
            }
         }
         break;
      case eCRLTT_OpenCurlyBracket:
         bracketDepth ++;
         break;
      case eCRLTT_ClosedCurlyBracket:
         if(bracketDepth > 0)
         {
            bracketDepth--;
         } 
         else
         {
            printf("[ERROR]: Bottomed out bracket at %d, %d\n", tokens[i].line, tokens[i].col);
         }
         break;
      }
   }
   
   CRLexer_Destroy(lexer);
}

static int StrEndsWith(const char * str, const char * match)
{
   size_t str_len, match_len;
   if(str == NULL || match == NULL)
   {
      return 0;
   }
   str_len = strlen(str);
   match_len = strlen(match);
   if(match_len > str_len)
   {
      return 0;
   }
   return strncmp(str + (str_len - match_len), match, match_len) == 0;
}

static void SearchAndAddFunctionsToData(struct RPath * path)
{
   struct RPathSet * pathSet;
   struct RPath * subPath;
   int i, size;
   const char * name;
   
   pathSet = RPathSet_MakeFromDirRP(path);
   size = RPathSet_Count(pathSet);
   //printf("Size: %d %s\n", size, RPath_Get(path));
   for(i = 0; i < size; i++)
   {
      subPath = RPathSet_Get(pathSet, i);
      name = RPath_GetFilename(subPath);
      //printf("Sname: %s\n", name);
      if(name[0] != '.' && strcmp(name, "CMakeFiles")     != 0 && 
                           strcmp(name, "tests")          != 0 &&
                           strcmp(name, "googletest-src") != 0 &&
                           strcmp(name, "BuildSystem")    != 0 &&
                           strcmp(name, "TestSystem")     != 0 &&
                           strcmp(name, "bsp")            != 0)
      {
         if(RPath_IsDirectory(subPath))
         {
            SearchAndAddFunctionsToData(subPath);
         }
         else if(StrEndsWith(name, ".h") || StrEndsWith(name, ".c"))
         {
            printf("File: %s\n", RPath_Get(subPath));
            AddFunctionCallsToData(RPath_Get(subPath));
         }
      }
   }
   
   
   RPathSet_Free(pathSet);
}

static void WriteGraphDotOutput(void)
{
   size_t i;
   struct CallRecord * record;
   FILE * fh;
   const char * filename = "output.dot";
   
   fh = fopen(filename, "w");
   if(fh == NULL)
   {
      fprintf(stderr, "Can't open file: %s\n", filename);
   }
   else
   {
      fprintf(fh, "digraph D {\n");
      
      for(i = 0; i < Data.count; i++)
      {
         record = &Data.records[i];
         //printf("R: %s %d %s %s %d\n", record->filename, record->insideFunctionLine, record->insideFunction, record->calledFunction, record->count);
         fprintf(fh, "   %s -> %s\n", record->insideFunction, record->calledFunction);
         
      }
      
      fprintf(fh, "}\n");
      fclose(fh);
   }
}

static void PrintRecords(void)
{
   size_t i;
   struct CallRecord * record;

   for(i = 0; i < Data.count; i++)
   {
      record = &Data.records[i];
      printf("R_%d: %s %d %s %s %d\n", (int)i, record->filename, record->insideFunctionLine, record->insideFunction, record->calledFunction, (int)record->count);
      
   }      
}

static int FindFunctionName(const char * functionName, const char * filename, int line, int startIndex)
{
   int foundIndex = -1;
   size_t i;
   struct CallRecord * record;   
   
   for(i = (size_t)startIndex; i < Data.count; i++)
   {
      record = &Data.records[i];
      if((strcmp(functionName, record->insideFunction) == 0) &&
         (filename == NULL || strcmp(filename, record->filename) == 0) &&
         (line <= 0 || line == record->insideFunctionLine))
      {
         foundIndex = (int)i;
         break;
      }
   }
   return foundIndex;
}
static int FindRootFunction(int startIndex, const char ** lastNameIgnore)
{
   int foundIndex = -1;
   size_t i, k;
   struct CallRecord * iRecord, *kRecord;   
   
   for(i = (size_t)startIndex; i < Data.count; i++)
   {
      int foundCaller = 0;
      iRecord = &Data.records[i];
      if(lastNameIgnore == NULL || *lastNameIgnore == NULL ||
         strcmp(*lastNameIgnore, iRecord->insideFunction) != 0)
      {
         for(k = 0; k < Data.count; k++)
         {
            kRecord = &Data.records[k];
            if(strcmp(iRecord->insideFunction, kRecord->calledFunction) == 0)
            {
               foundCaller = 1;
               break;
            }
         }
         
         if(!foundCaller)
         {
            foundIndex = i;
            if(lastNameIgnore != NULL)
            {
               (*lastNameIgnore) = iRecord->insideFunction;
            }
            break;
         }
      }
   }
   return foundIndex;
}

struct TreeCallbackFunctions
{
   void (*WriteHeader)(FILE *fh);
   void (*WriteFooter)(FILE *fh);
   void (*WriteFunctionBegin)(FILE *fh, int nodeID, const char * functionName, const char * filename, int line, int recursionProtection);
   void (*WriteFunctionEnd)(FILE *fh, int nodeID);
   void (*WriteLink)(FILE *fh, int fromNodeID, int toNodeID);
   int addRootFlag;
};

struct TreeData
{
   FILE * fh;
   int nextNewNodeID;
   int * functionIndexStack;
   int functionIndexStackPtr;
   struct TreeCallbackFunctions * cbfs;
};

// Tree Dot Callbacks
static void DotCallback_WriteHeader(FILE *fh)
{
   fprintf(fh, "digraph D {\n");
}
static void DotCallback_WriteFooter(FILE *fh)
{
   fprintf(fh, "}\n");
}
static void DotCallback_WriteFunctionBegin(FILE *fh, int nodeID, const char * functionName, const char * filename, int line, int recursionProtection)
{
   fprintf(fh, "   n%d [label=\"%s\"];\n", nodeID, functionName);   
}
static void DotCallback_WriteFunctionEnd(FILE *fh, int nodeID)
{
   // Intentionaly Empty
}
static void DotCallback_WriteLink(FILE *fh, int fromNodeID, int toNodeID)
{
   fprintf(fh, "   n%d -> n%d;\n", fromNodeID, toNodeID);
}

static struct TreeCallbackFunctions DotCallbackFunctions = {
   DotCallback_WriteHeader,
   DotCallback_WriteFooter,
   DotCallback_WriteFunctionBegin,
   DotCallback_WriteFunctionEnd,
   DotCallback_WriteLink,
   0
};


// Tree MindMap Callbacks
static void MindMapCallback_WriteHeader(FILE *fh)
{
   fprintf(fh, "<map version=\"freeplane 1.8.0\">\n");
   fprintf(fh, "<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->\n");
}
static void MindMapCallback_WriteFooter(FILE *fh)
{
   fprintf(fh, "</map>\n");
}
static void MindMapCallback_WriteFunctionBegin(FILE *fh, int nodeID, const char * functionName, const char * filename, int line, int recursionProtection)
{
   fprintf(fh, "<node TEXT=\"%s\" FOLDED=\"true\" ID=\"ID_%d\">\n",functionName, nodeID);
   if(filename != NULL)
   {
      fprintf(fh, "<node TEXT=\"File: %s Line: %d\"/>\n",filename, line);
   }
   if(recursionProtection)
   {
      fprintf(fh, "<node TEXT=\"[Recursive Function Call Detected]\"/>\n");
   }

}
static void MindMapCallback_WriteFunctionEnd(FILE *fh, int nodeID)
{
   fprintf(fh, "</node>\n");
}
static void MindMapCallback_WriteLink(FILE *fh, int fromNodeID, int toNodeID)
{
   // Intentionaly Empty
}

static struct TreeCallbackFunctions MindMapCallbackFunctions = {
   MindMapCallback_WriteHeader,
   MindMapCallback_WriteFooter,
   MindMapCallback_WriteFunctionBegin,
   MindMapCallback_WriteFunctionEnd,
   MindMapCallback_WriteLink,
   1
};

static void TreeDotFIS_Init(struct TreeData * treeData, size_t stackSizeToAllocate)
{
   treeData->functionIndexStack = malloc(sizeof(int) * stackSizeToAllocate);
   treeData->functionIndexStackPtr = 0;
}

static void TreeDotFIS_Destroy(struct TreeData * treeData)
{
   free(treeData->functionIndexStack);
   treeData->functionIndexStackPtr = 0;
}

static void TreeDotFIS_Push(struct TreeData * treeData, int functionIndex)
{
   treeData->functionIndexStack[treeData->functionIndexStackPtr] = functionIndex;
   treeData->functionIndexStackPtr ++;
}

static int TreeDotFIS_Pop(struct TreeData * treeData)
{
   treeData->functionIndexStackPtr --;
   return treeData->functionIndexStack[treeData->functionIndexStackPtr];
}

static int TreeDotFIS_Contains(struct TreeData * treeData, int functionIndex)
{
   int i;
   int found = 0;
   struct CallRecord * stackRecord, *record;
   for(i = 0; i < treeData->functionIndexStackPtr; i++)
   {
      stackRecord = &Data.records[treeData->functionIndexStack[i]];
      record      = &Data.records[functionIndex];
      if(strcmp(record->insideFunction, stackRecord->insideFunction) == 0)
      {
         found = 1;
         break;
      }
   }
   return found;
}
static void WriteTreeFunction(struct TreeData * treeData, int firstFunctionIndex, int parentNodeID );

static int WriteTreeCalledFunctions(struct TreeData * treeData, int firstFunctionIndex, int parentNodeID, int writeChilderenFlag)
{
   int foundIndex;
   int thisNodeID;
   int lastGoodIndex = -1;
   struct CallRecord * record; 


   record = &Data.records[firstFunctionIndex];
   thisNodeID = treeData->nextNewNodeID;
   treeData->nextNewNodeID ++;
   treeData->cbfs->WriteFunctionBegin(treeData->fh, thisNodeID, record->insideFunction, record->filename, record->insideFunctionLine, !writeChilderenFlag);

   // Find each function call record
   foundIndex = firstFunctionIndex;
   while(foundIndex >= 0)
   {
      int childNodeID;
      int childFunctionIndex;
      record = &Data.records[foundIndex];
      //printf("FoundIndex: %d\n", foundIndex); 
      childFunctionIndex = FindFunctionName(record->calledFunction, NULL, -1, 0);
      //printf("Found Child Index: %d, %s\n", childFunctionIndex, record->calledFunction);
      if(writeChilderenFlag)
      {
         if(childFunctionIndex >= 0)
         {
            WriteTreeFunction(treeData, childFunctionIndex, thisNodeID);
         }
         else
         {
            childNodeID = treeData->nextNewNodeID;
            treeData->nextNewNodeID ++;
            treeData->cbfs->WriteFunctionBegin(treeData->fh, childNodeID, record->calledFunction, NULL, -1, 0);
            treeData->cbfs->WriteFunctionEnd(treeData->fh, childNodeID);
            treeData->cbfs->WriteLink(treeData->fh, thisNodeID, childNodeID);         
         }
      }
      
       
      lastGoodIndex = foundIndex;
      foundIndex = FindFunctionName(record->insideFunction, record->filename, record->insideFunctionLine, foundIndex + 1);
   }
   
   treeData->cbfs->WriteFunctionEnd(treeData->fh, thisNodeID);
   if(parentNodeID >= 0)
   {
      treeData->cbfs->WriteLink(treeData->fh, parentNodeID, thisNodeID); 
   }
   
   
   return lastGoodIndex;
}

static void WriteTreeFunction(struct TreeData * treeData, int firstFunctionIndex, int parentNodeID)
{
   int foundIndex;
   struct CallRecord * record; 
   int writeChilderenFlag;
   record = &Data.records[firstFunctionIndex];

   if( TreeDotFIS_Contains(treeData, firstFunctionIndex))
   {
      writeChilderenFlag = 0;
   }
   else
   {
      TreeDotFIS_Push(treeData, firstFunctionIndex);
      writeChilderenFlag = 1;
   }
      
   firstFunctionIndex = WriteTreeCalledFunctions(treeData, firstFunctionIndex, parentNodeID, writeChilderenFlag);
   while(firstFunctionIndex >= 0)
   {
      //printf("%d: %s\n", firstFunctionIndex, record->insideFunction);
      firstFunctionIndex = FindFunctionName(record->insideFunction, NULL, -1, firstFunctionIndex + 1);
      if(firstFunctionIndex >= 0)
      {
         firstFunctionIndex = WriteTreeCalledFunctions(treeData, firstFunctionIndex, parentNodeID, writeChilderenFlag);
      }
   }
   
   if(writeChilderenFlag)
   {
      TreeDotFIS_Pop(treeData);
   }
}


static void WriteAllRootElements(struct TreeData * treeData)
{
   int startfunctionIndex = 0;
   int rootID;
   const char * lastNameIgnore = NULL;
   
   if(treeData->cbfs->addRootFlag)
   {
      treeData->cbfs->WriteFunctionBegin(treeData->fh, 0, "All Root Functions", NULL, -1, 0);
      rootID = 0;
   }
   else
   {
      rootID = -1;
   }
   while((startfunctionIndex = FindRootFunction(startfunctionIndex, &lastNameIgnore)) >= 0)
   {
      WriteTreeFunction(treeData, startfunctionIndex, rootID);
      startfunctionIndex ++;
   }
   if(treeData->cbfs->addRootFlag)
   {
      treeData->cbfs->WriteFunctionEnd(treeData->fh, 0);
   }
}

static void WriteTreeOutput(struct TreeCallbackFunctions * treeCallbackFunctions, const char * filename)
{
   FILE * fh;
   struct TreeData treeData;
   
   fh = fopen(filename, "w");
   
   if(fh == NULL)
   {
      fprintf(stderr, "Can't open file for writting: %s\n", filename);
   }
   else
   {
      treeData.fh = fh;
      treeData.nextNewNodeID = 1;
      treeData.cbfs = treeCallbackFunctions;
      TreeDotFIS_Init(&treeData, Data.count);
      
      
      treeData.cbfs->WriteHeader(fh);
      WriteAllRootElements(&treeData);


      
      treeData.cbfs->WriteFooter(fh);
      
      TreeDotFIS_Destroy(&treeData);
      fclose(fh);
   }
}

int main(int argc, char * args[])
{
   struct RPath * path;
   
   Data_Init();
   path = RPath_Make(args[1]);
   //TokenizeFile("CRLexer.c");
   //PrintFunctionCalls("CRLexer.c");
   SearchAndAddFunctionsToData(path);

   RPath_Free(path);
   
   WriteGraphDotOutput();
   //PrintRecords();
   WriteTreeOutput(&DotCallbackFunctions,     "treeOutput.dot");
   WriteTreeOutput(&MindMapCallbackFunctions, "treeOutput.mm");
   Data_Destroy();
   
   printf("End\n");
   return 0;
}
