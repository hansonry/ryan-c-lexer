#ifndef __RYANPATH_H__
#define __RYANPATH_H__


struct RPath;

struct RPathSet;


struct RPath * RPath_Make(const char * path);
struct RPath * RPath_Copy(struct RPath * path);
void RPath_Free(struct RPath * path);

int RPath_IsAbsolute(struct RPath * path);
void RPath_Append(struct RPath * path, const char * toAppend);
void RPath_AppendRP(struct RPath * path, struct RPath * toAppend);

void RPath_Normalize(struct RPath * path);

const char * RPath_Get(struct RPath * path);

const char * RPath_GetFilename(struct RPath * path);

int RPath_Exists(struct RPath * path);

int RPath_IsFile(struct RPath * path);

int RPath_IsDirectory(struct RPath * path);

const char * RPath_GetExtention(struct RPath * path);

struct RPathSet * RPathSet_MakeFromDir(const char * dirPath);
struct RPathSet * RPathSet_MakeFromDirRP(struct RPath * dirPath);
void RPathSet_Free(struct RPathSet * pathSet);

struct RPath * RPathSet_Get(struct RPathSet * pathSet, int index);
int RPathSet_Count(struct RPathSet * pathSet);

#endif // __RYANPATH_H__

