#ifndef __CRLEXER_H__
#define __CRLEXER_H__
#include <stddef.h>


struct CRLexer;

enum CRLTokenType
{
   eCRLTT_Unknown,
   eCRLTT_Literal,
   eCRLTT_Number,
   eCRLTT_String,
   eCRLTT_Character,
   eCRLTT_Plus,
   eCRLTT_Minus,
   eCRLTT_Asterisk,
   eCRLTT_Divide,
   eCRLTT_PlusEquals,
   eCRLTT_MinusEquals,
   eCRLTT_DivideEquals,
   eCRLTT_MultiplyEquals,
   eCRLTT_Increment,
   eCRLTT_Decrement,
   eCRLTT_Comma,
   eCRLTT_Dot,
   eCRLTT_DereferenceDot,
   eCRLTT_Colon,
   eCRLTT_OpenParen,
   eCRLTT_ClosedParen,
   eCRLTT_OpenSquareBraket,
   eCRLTT_ClosedSquareBraket,
   eCRLTT_OpenCurlyBracket,
   eCRLTT_ClosedCurlyBracket,
   eCRLTT_Semicolon,
   eCRLTT_Equals,
   eCRLTT_DoubleEquals,
   eCRLTT_LessThan,
   eCRLTT_GreaterThan,
   eCRLTT_LessThanOrEqualTo,
   eCRLTT_GreaterThanOrEqualTo,
   eCRLTT_NotEqual,
   eCRLTT_BitShiftLeft,
   eCRLTT_BitShiftRight,
   eCRLTT_BitwiseNot,
   eCRLTT_BitwiseAnd,
   eCRLTT_BitwiseOr,
   eCRLTT_LogicalNot,
   eCRLTT_LogicalAnd,
   eCRLTT_LogicalOr,
   eCRLTT_Struct,
   eCRLTT_Enum,
   eCRLTT_Union,
   eCRLTT_Typedef,
   eCRLTT_If,
   eCRLTT_For,
   eCRLTT_Do,
   eCRLTT_While,
   eCRLTT_Goto,
   eCRLTT_Switch,
   eCRLTT_Case,
   eCRLTT_Break,
   eCRLTT_Static,
   eCRLTT_Extern,
   eCRLTT_Void,
   eCRLTT_Const,
   eCRLTT_Return,
};

struct CRLToken
{
   enum CRLTokenType type;
   char * text;
   int line;
   int col;
};

struct CRLexer * CRLexer_New(const char * filename);
void CRLexer_Destroy(struct CRLexer * lexer);

struct CRLToken * CRLexer_GetTokens(struct CRLexer * lexer, size_t * count);



#endif // __CRLEXER_H__
