#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "RyanPath.h"

struct RP_Link
{
   char * text;
   int isBase;
   struct RP_Link * next;
};

struct RPath
{
   char * textBuffer;
   size_t tbSize;
   int tbDirty;
   struct RP_Link * root;
   const char * seperator;
};

static void RPath_Dump(struct RPath * path)
{
   struct RP_Link * tLink;
   
   printf("PATH { ");
   if(path->root == NULL)
   {
      printf("(NULL ROOT)");
   }
   
   tLink = path->root;
   while(tLink != NULL)
   {
      printf("Link {");
      if(tLink->text == NULL)
      {
         printf("text:(NULL) ");
      }
      else
      {
         printf("text:\"%s\" ", tLink->text);
      }
      printf("isBase: %d }", tLink->isBase);
      tLink = tLink->next;
   }
   
   printf("\n");
}

static char * RPath_CopyStrSection(const char * str, int start, int current)
{
   size_t size = current - start + 1;
   char * nStr;
   nStr = malloc(size + 1);
   memcpy(nStr, &str[start], size);
   nStr[size] = '\0';
   return nStr;
}


static void RPath_AddLink(struct RPath * path, struct RP_Link * link, int copyText)
{
   struct RP_Link * cLink, *tLink;
   cLink = malloc(sizeof(struct RP_Link));
   memcpy(cLink, link, sizeof(struct RP_Link));
   if(copyText)
   {
      if(link->text == NULL)
      {
         cLink->text = NULL;
      }
      else
      {
         cLink->text = strdup(link->text);
      }
   }
   cLink->next = NULL;
   if(path->root == NULL)
   {
      path->root = cLink;
   }
   else
   {
      tLink = path->root;
      while(tLink->next != NULL)
      {
         tLink = tLink->next;
      }
      
      tLink->next = cLink;
   }
}

static void RPath_Populate(struct RPath * path, const char * pathText)
{
   const char * ptc;
   int startIndex, index;
   int isBase = 0;
   struct RP_Link link;
   startIndex = -1;
   index = 0;
   
   while(pathText[index] != '\0')
   {
      char c = pathText[index];
      if(c == '/' || c == '\\')
      {
         if(path->seperator == NULL)
         {
            if(c == '/')
            {
               path->seperator = "/";
            }
            else if(c == '\\')
            {
               path->seperator = "\\";
            }
         }
         
         if(index == 0)
         {
            // This is a nix Base
            startIndex = 1;
            link.text = NULL;
            link.isBase = 1;
            RPath_AddLink(path, &link, 0);
         }
         else if(startIndex >= 0)
         {
            // standard seperator
            link.text = RPath_CopyStrSection(pathText, startIndex, index - 1);
            link.isBase = isBase;
            RPath_AddLink(path, &link, 0);
            isBase = 0;
            startIndex = -1;
            
         }
      }
      else
      {
         if(startIndex < 0)
         {
            startIndex = index;
         }
         
         if(c == ':' && index == 1)
         {
            // Windows Base
            isBase = 1;
         }
      }
      
      index ++;
   }

   if(startIndex >= 0)
   {
      link.text = RPath_CopyStrSection(pathText, startIndex, index - 1);
      link.isBase = isBase;
      RPath_AddLink(path, &link, 0);
   }      
}

struct RPath * RPath_Make(const char * path)
{
   struct RPath * newPath;
   newPath = malloc(sizeof(struct RPath));
   newPath->textBuffer = NULL;
   newPath->tbSize = 0;
   newPath->tbDirty = 1;
   newPath->root = NULL;
   newPath->seperator = NULL;
   RPath_Populate(newPath, path);
   return newPath;
}

struct RPath * RPath_Copy(struct RPath * path)
{
   struct RPath * newPath;
   struct RP_Link *tLink;
   newPath = malloc(sizeof(struct RPath));
   newPath->textBuffer = NULL;
   newPath->tbSize = 0;
   newPath->tbDirty = 1;
   newPath->root = NULL;
   newPath->seperator = path->seperator;
   
   RPath_AppendRP(newPath, path);

   return newPath;
}

static void RPath_FreeLink(struct RP_Link * link)
{
   if(link->text != NULL)
   {
      free(link->text);
   }
   free(link);
}

void RPath_Free(struct RPath * path)
{
   struct RP_Link *tLink, *dLink;
   tLink = path->root;
   while(tLink != NULL)
   {
      dLink = tLink;
      tLink = tLink->next;
      RPath_FreeLink(dLink);
   }
   if(path->textBuffer != NULL)
   {
      free(path->textBuffer);
   }
   free(path);
}

int RPath_IsAbsolute(struct RPath * path)
{
   if(path->root == NULL)
   {
      return 0;
   }
   return path->root->isBase;
}

void RPath_Append(struct RPath * path, const char * toAppend)
{
   struct RPath * toAppendPath;
   toAppendPath = RPath_Make(toAppend);
   RPath_AppendRP(path, toAppendPath);
   RPath_Free(toAppendPath);
}

void RPath_AppendRP(struct RPath * path, struct RPath * toAppend)
{
   struct RP_Link *tLink;

   tLink = toAppend->root;
   while(tLink != NULL)
   {
      RPath_AddLink(path, tLink, 1);
      tLink = tLink->next;
   }
   path->tbDirty = 1;
}



void RPath_Normalize(struct RPath * path)
{
   struct RP_Link *tLink, *pLink, *ppLink;
   pLink = NULL;
   tLink = path->root;
   while(tLink != NULL)
   {
      if(tLink->text != NULL && 
         strcmp(tLink->text, ".") == 0)
      {
         if(pLink == NULL)
         {
            path->root = tLink->next;
         }
         else
         {
            pLink->next = tLink->next;
         }
         RPath_FreeLink(tLink);
         path->tbDirty = 1;
      }
      pLink = tLink;
      tLink = tLink->next;
   }
   
   pLink = NULL;
   ppLink = NULL;
   tLink = path->root;
   while(tLink != NULL)
   {
      if(tLink->text != NULL && strcmp(tLink->text, "..") == 0 && 
         pLink != NULL && 
         pLink->text != NULL && strcmp(pLink->text, "..") != 0)
      {
         if(ppLink == NULL)
         {
            path->root = tLink->next;
         }
         else
         {
            ppLink->next = tLink->next;
         }
         
         RPath_FreeLink(tLink);
         RPath_FreeLink(pLink);
         path->tbDirty = 1;
         
         // Reset everything to NULL and restart the loop
         pLink = NULL;
         ppLink = NULL;
         tLink = path->root;
      }
      else
      {
         ppLink = pLink;
         pLink  = tLink;
         tLink  = tLink->next;
      }
   }
}


static void RPath_BuildTextBuffer(struct RPath * path)
{
   size_t strSize = 0;
   struct RP_Link *tLink;
   size_t seperateSize;
   size_t tSize;
   char * c;
   

   if(path->seperator == NULL)
   {
      path->seperator = "/";
   }
   seperateSize = strlen(path->seperator);
   
   // Compute Size   
   tLink = path->root;
   while(tLink != NULL)
   {
      if(tLink->text != NULL)
      {
         strSize += strlen(tLink->text);
      }
      if(tLink->next != NULL)
      {
         strSize += seperateSize;
      }
      tLink = tLink->next;
   }

   // Resize to fit
   path->textBuffer = realloc(path->textBuffer, strSize + 1);
   
   // Insert text
   tLink = path->root;
   c = path->textBuffer;
   while(tLink != NULL)
   {
      if(tLink->text != NULL)
      {
         tSize = strlen(tLink->text);
         memcpy(c, tLink->text, tSize);
         c += tSize;
      }
      
      if(tLink->next != NULL)
      {     
         memcpy(c, path->seperator, seperateSize);
         c += seperateSize;
      }
      tLink = tLink->next;
   }
   
   c[0] = '\0';
   
   path->tbDirty = 0;
}

const char * RPath_Get(struct RPath * path)
{
   if(path->tbDirty)
   {
      RPath_BuildTextBuffer(path);  
   }
   return path->textBuffer;
}

const char * RPath_GetFilename(struct RPath * path)
{
   
   struct RP_Link *tLink;
   
   if(path->root == NULL)
   {
      return "";
   }
   
   tLink = path->root;
   while(tLink->next != NULL) tLink = tLink->next;
   
   if(tLink->text == NULL)
   {
      return "";
   }
   return tLink->text;
   
}

int RPath_Exists(struct RPath * path)
{
   return access(RPath_Get(path), F_OK) != -1;   
}

int RPath_IsFile(struct RPath * path)
{
   struct stat path_stat;
   stat(RPath_Get(path), &path_stat);
   return S_ISREG(path_stat.st_mode);
}

int RPath_IsDirectory(struct RPath * path)
{
   struct stat path_stat;
   stat(RPath_Get(path), &path_stat);
   return S_ISDIR(path_stat.st_mode); 
}

const char * RPath_GetExtention(struct RPath * path)
{
   struct RP_Link *tLink;
   char * c;
   
   if(path->root == NULL)
   {
      return "";
   }
   tLink = path->root;
   while(tLink->next != NULL) tLink = tLink->next;
   
   if(tLink->text == NULL)
   {
      return "";
   }
   c = strrchr(tLink->text, '.');
   if(c == NULL)
   {
      return "";
   }
   return c;
}

struct RPathSet
{
   struct RPath ** base;
   size_t size;
   size_t count;
};

static void RPathSet_Add(struct RPathSet * pathSet, struct RPath * path)
{
   if(pathSet->count >= pathSet->size)
   {
      pathSet->size = pathSet->count + 32;
      pathSet->base = realloc(pathSet->base, sizeof(struct RPathSet *) * pathSet->size);
   }
   
   pathSet->base[pathSet->count] = path;
   pathSet->count ++;
}

struct RPathSet * RPathSet_MakeFromDir(const char * dirPath)
{
   struct RPath *base;
   struct RPathSet * pathSet;
   
   base = RPath_Make(dirPath);
   pathSet = RPathSet_MakeFromDirRP(base);
   RPath_Free(base);
   
   return pathSet;
}

struct RPathSet * RPathSet_MakeFromDirRP(struct RPath * dirPath)
{
   DIR * dh;
   struct dirent * entry;
   struct RPath *sub;
   struct RPathSet * pathSet;

   dh = opendir(RPath_Get(dirPath));
   if(dh == NULL)
   {
      return NULL;
   }
   
   pathSet = malloc(sizeof(struct RPathSet));
   pathSet->base = NULL;
   pathSet->size = 0;
   pathSet->count = 0;
   
   while((entry = readdir(dh)) != NULL)
   {
      if(strcmp(entry->d_name, ".") != 0 &&
         strcmp(entry->d_name, "..") != 0)
      {
         sub = RPath_Copy(dirPath);
         RPath_Append(sub, entry->d_name);
         RPathSet_Add(pathSet, sub);
      }
   }
     
   closedir(dh);
   return pathSet;   
}

void RPathSet_Free(struct RPathSet * pathSet)
{
   size_t i;
   if(pathSet->base != NULL)
   {
      for(i = 0; i < pathSet->count; i ++)
      {
         RPath_Free(pathSet->base[i]);
      }
      free(pathSet->base);
   }
   free(pathSet);
}

struct RPath * RPathSet_Get(struct RPathSet * pathSet, int index)
{
   if(index >= pathSet->count)
   {
      return NULL;
   }
   return pathSet->base[index];
}

int RPathSet_Count(struct RPathSet * pathSet)
{
   return pathSet->count;
}

