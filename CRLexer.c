#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "CRLexer.h"

struct TokenTextPair
{
   enum CRLTokenType type;
   const char * text;
};

static struct TokenTextPair TokenTextPairs[] = {
   { eCRLTT_Plus,                  "+" },
   { eCRLTT_Minus,                 "-" },
   { eCRLTT_Asterisk,              "*" },
   { eCRLTT_Divide,                "/" },
   { eCRLTT_PlusEquals,            "+=" },
   { eCRLTT_MinusEquals,           "-=" },
   { eCRLTT_DivideEquals,          "/=" },
   { eCRLTT_MultiplyEquals,        "*=" },
   { eCRLTT_Increment,             "++" },
   { eCRLTT_Decrement,             "--" },
   { eCRLTT_Comma,                 "," },
   { eCRLTT_Dot,                   "." },
   { eCRLTT_DereferenceDot,        "->" },
   { eCRLTT_Colon,                 ":" },
   { eCRLTT_OpenParen,             "(" },
   { eCRLTT_ClosedParen,           ")" },
   { eCRLTT_OpenSquareBraket,      "[" },
   { eCRLTT_ClosedSquareBraket,    "]" },
   { eCRLTT_OpenCurlyBracket,      "{" },
   { eCRLTT_ClosedCurlyBracket,    "}" },
   { eCRLTT_Semicolon,             ";" },
   { eCRLTT_Equals,                "=" },
   { eCRLTT_DoubleEquals,          "==" },
   { eCRLTT_LessThan,              "<" },
   { eCRLTT_GreaterThan,           ">" },
   { eCRLTT_LessThanOrEqualTo,     "<=" },
   { eCRLTT_GreaterThanOrEqualTo,  ">=" },
   { eCRLTT_NotEqual,              "!=" },
   { eCRLTT_BitShiftLeft,          "<<" },
   { eCRLTT_BitShiftRight,         ">>" },
   { eCRLTT_BitwiseNot,            "~" },
   { eCRLTT_BitwiseAnd,            "&" },
   { eCRLTT_BitwiseOr,             "|" },
   { eCRLTT_LogicalNot,            "!" },
   { eCRLTT_LogicalAnd,            "&&" },
   { eCRLTT_LogicalOr,             "||" },
   { eCRLTT_Struct,                "struct" },
   { eCRLTT_Enum,                  "enum" },
   { eCRLTT_Union,                 "union" },
   { eCRLTT_Typedef,               "typedef" },
   { eCRLTT_If,                    "if" },
   { eCRLTT_For,                   "for" },
   { eCRLTT_Do,                    "do" },
   { eCRLTT_While,                 "while" },
   { eCRLTT_Goto,                  "goto" },
   { eCRLTT_Switch,                "switch" },
   { eCRLTT_Case,                  "case" },
   { eCRLTT_Break,                 "break" },
   { eCRLTT_Static,                "static" },
   { eCRLTT_Extern,                "extern" },
   { eCRLTT_Void,                  "void" }, 
   { eCRLTT_Const,                 "const" },
   { eCRLTT_Return,                "return" },
   { eCRLTT_Unknown,               NULL }
};

struct CRLexer
{
   struct CRLToken * tokens;
   size_t size;
   size_t count;
};

struct InputSys
{
   char * all;
   size_t size;
   size_t next;
   int line;
   int col;
};

#define FILE_READ_CHUNKSIZE 512
static char * ReadFile(FILE *fh, size_t * sizePtr)
{
   size_t count = 0;
   size_t size = 0;
   size_t read = FILE_READ_CHUNKSIZE;
   char * text = NULL;
   
   while(read == FILE_READ_CHUNKSIZE)
   {
      size += FILE_READ_CHUNKSIZE;
      text = realloc(text, size);
      read = fread(text + count, 1, FILE_READ_CHUNKSIZE, fh);
      count += read;
         
   }
   
   // Resize for the null termnation
   if(count == size)
   {
      text = realloc(text, size + 1);
   }
   
   text[count] = '\0';
   if(sizePtr != NULL)
   {
      (*sizePtr) = count;
   }
   
   return text;
}



static void InputSys_InitFromFile(struct InputSys * is, FILE *fh)
{

   is->all = ReadFile(fh, &is->size);
   
   // Set default Values
   is->next = 0;
   is->line = 1;
   is->col  = 1;
}

static void InputSys_Destroy(struct InputSys * is)
{
   free(is->all);
   is->all = NULL;
   is->next = 0;
}



static int InputSys_Peek(struct InputSys * is, int * line, int * col)
{
   if(is->next >= is->size)
   {
      return -1;
   }
   
   if(col != NULL)
   {
      (*col) = is->col;
   }
   if(line != NULL)
   {
      (*line) = is->line;
   }
   return is->all[is->next];
}

static int InputSys_Next(struct InputSys * is, int * line, int * col)
{
   int v;
   v = InputSys_Peek(is, line, col);
   
   if(v >= 0)
   {
      is->next++;
      
      is->col ++;
      if(v == '\n')
      {
         is->col = 1;
         is->line ++;
      }
   }
   
   return v;
}

struct StringBuffer
{
   size_t size;
   size_t count;
   char * base;
};
#define STRING_BUFFER_GROWBY 16
static void StringBuffer_Init(struct StringBuffer * buf)
{
   buf->size = STRING_BUFFER_GROWBY;
   buf->count = 0;
   buf->base = malloc(buf->size);
}

static void StringBuffer_Destory(struct StringBuffer * buf)
{
   free(buf->base);
   buf->base = NULL;
   buf->count = 0;
   buf->size = 0;
}

static void StringBuffer_Add(struct StringBuffer * buf, int c)
{
   if(c >= 0)
   {
      if(buf->count >= buf->size)
      {
         buf->size += STRING_BUFFER_GROWBY;
         buf->base = realloc(buf->base, buf->size);
      }
      
      buf->base[buf->count] = (char)c;
      buf->count ++;
   }
}

static char * StringBuffer_StringCopy(struct StringBuffer * buf)
{
   char * newStr;
   newStr = malloc(buf->count + 1);
   memcpy(newStr, buf->base, buf->count);
   newStr[buf->count] = '\0';
   return newStr;
}

static int StringBuffer_CheckMatch(struct StringBuffer * buf, const char * str)
{
   size_t strLen;
   strLen = strlen(str);
   if(buf->count > strLen)
   {
      return 0;
   }
   
   if(memcmp(buf->base, str, buf->count) == 0)
   {
      if(buf->count == strLen)
      {
         return 1;
      }
      else
      {
         return 2;
      }
   }
   return 0;
}

static void StringBuffer_UndoAdd(struct StringBuffer * buf)
{
   if(buf->count > 0)
   {
      buf->count --;
   }
}

static void StringBuffer_Clear(struct StringBuffer * buf)
{
   buf->count = 0;
}


static int IsWhitespace(int c)
{
   return c == ' '  || 
          c == '\t' ||
          c == '\n' ||
          c == '\r';          
}

static int IsDigit(int c)
{
   return c >= '0' && c <= '9';
}
static int IsText(int c)
{
   return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_';
}

#define TOKEN_LIST_GROWBY 32
static void CRL_InitTokens(struct CRLexer * lexer)
{
   lexer->tokens = malloc(sizeof(struct CRLToken) * TOKEN_LIST_GROWBY);
   lexer->size = TOKEN_LIST_GROWBY;
   lexer->count = 0;
}

static void CRL_AddToken(struct CRLexer * lexer, struct CRLToken * token)
{
   if(lexer->count >= lexer->size)
   {
      lexer->size += TOKEN_LIST_GROWBY;
      lexer->tokens = realloc(lexer->tokens, sizeof(struct CRLToken) * lexer->size);
   }
   
   memcpy(&lexer->tokens[lexer->count], token, sizeof(struct CRLToken));
   lexer->count++;
   
}

static void CRL_FreeTokens(struct CRLexer * lexer)
{
   size_t i;
   for(i = 0; i < lexer->count; i++)
   {
      free(lexer->tokens[i].text);
   }
   free(lexer->tokens);
   lexer->size = 0;
   lexer->count = 0;
}

static enum CRLTokenType CRL_SymbolLookup(struct StringBuffer * buf)
{
   size_t i = 0;
   enum CRLTokenType type = eCRLTT_Unknown;
   while(TokenTextPairs[i].text != NULL)
   {
      int m = StringBuffer_CheckMatch(buf, TokenTextPairs[i].text);
      if(m == 1)
      {
         type = TokenTextPairs[i].type;
         break;
      }
      i++;
   }
   
   return type;
}

static int CRL_SymbolLookupPartial(struct StringBuffer * buf, int startIndex)
{
   size_t i = (size_t)startIndex;
   int found = -1;
   while(TokenTextPairs[i].text != NULL)
   {
      int m = StringBuffer_CheckMatch(buf, TokenTextPairs[i].text);
      if(m == 2 || m == 1)
      {
         found = i;
         break;
      }
      i++;
   }
   
   return found;
}


static void CRL_IgnoreToEOL(struct InputSys * is)
{
   int c;
   while((c = InputSys_Next(is, NULL, NULL)) >= 0 && c != '\n');
}

static void CRL_IgnoreToEndOfMacro(struct InputSys * is)
{
   int c;
   int nextLineFlag = 0;
   while((c = InputSys_Next(is, NULL, NULL)) >= 0)
   {
      if(c == '\n')
      {
         if(nextLineFlag)
         {
            nextLineFlag = 0;
         }
         else
         {
            break;
         }
      }
      else if(c == '\\')
      {
         nextLineFlag = 1;
      }
   }
}

static void CRL_IgnoreUntilIncluding(struct InputSys * is, const char * str)
{
   int matchIndex = 0;
   int c;
   while(str[matchIndex] != '\0' && (c = InputSys_Next(is, NULL, NULL)) >= 0)
   {
      if(str[matchIndex] == c)
      {
         matchIndex ++;
      }
      else
      {
         matchIndex = 0;
      }
   }
}

static void CRL_IgnoreToNonWhitespace(struct InputSys * is)
{
   int c;
   while((c = InputSys_Peek(is, NULL, NULL)) >= 0 )
   {
      if(IsWhitespace(c))
      {
         (void)InputSys_Next(is, NULL, NULL);
      }
      else
      {
         break;
      }
   }
}

static void CRL_LexSlash(struct CRLexer * lexer, 
                         struct InputSys * is, 
                         struct StringBuffer * buf)
{
   int c;
   struct CRLToken token;
  
   c = InputSys_Next(is, &token.line, &token.col);
   StringBuffer_Add(buf, c);
  
   c = InputSys_Peek(is, NULL, NULL);
   if(c == '/')
   {
      CRL_IgnoreToEOL(is);
   }
   else if(c == '*')
   {
      StringBuffer_Clear(buf);
      CRL_IgnoreUntilIncluding(is, "*/");
   }
   else if(c == '=')
   {
      c = InputSys_Next(is, NULL, NULL);
      StringBuffer_Add(buf, c);
      token.type = eCRLTT_DivideEquals;

      token.text = StringBuffer_StringCopy(buf);
      CRL_AddToken(lexer, &token);
   }
   else
   {
      token.type = eCRLTT_Divide;
      token.text = StringBuffer_StringCopy(buf);
      CRL_AddToken(lexer, &token);
   }
}

static void CRL_LexNumber(struct CRLexer * lexer, 
                          struct InputSys * is, 
                          struct StringBuffer * buf,
                          int line, int col)
{
   struct CRLToken token;
   int exponentFlag = 0;
   int xIsAllowed = 1;
   int eIsAllowed = 1;
   int isFloat = 0;
   int inLetterFlags = 0;
   int c;

   while((c = InputSys_Peek(is, NULL, NULL)) >= 0)
   {
      if(!inLetterFlags && (c == '+' || c == '-'))
      {
         if(exponentFlag)
         {
            exponentFlag = 0;
            (void)InputSys_Next(is, NULL, NULL);
            StringBuffer_Add(buf, c);
         }
         else
         {
            // End of Number Detected
            break;
         }
      }
      else if(IsWhitespace(c) || (exponentFlag && !eIsAllowed))
      {
         break;
      }
      else if(!inLetterFlags && 
              (IsDigit(c) || c == '.'))
      {
         c = InputSys_Next(is, NULL, NULL);
         StringBuffer_Add(buf, c);
         if(c != '0')
         {
            xIsAllowed = 0;
         }
         if(c == '.')
         {
            isFloat = 1;
         }
      }
      else if(!inLetterFlags && (c == 'x' || c == 'X'))
      {
         if(xIsAllowed)
         {
            (void)InputSys_Next(is, NULL, NULL);
            StringBuffer_Add(buf, 'x');
            xIsAllowed = 0;
         }
         else
         {
            break;
         }
      }
      else if(!inLetterFlags && (c == 'e' || c == 'E'))
      {
         if(eIsAllowed)
         {
            eIsAllowed = 0;
            exponentFlag = 1;
            isFloat = 1;
            (void)InputSys_Next(is, NULL, NULL);
            StringBuffer_Add(buf, 'e'); 
         }
         else
         {
            break;
         }
      }
      else if(c == 'L' || c == 'l' || 
              c == 'U' || c == 'u' || 
              c == 'F' || c == 'f')
      {
         inLetterFlags = 1;
         (void)InputSys_Next(is, NULL, NULL);
         StringBuffer_Add(buf, c); 
      }
      else
      {
         break;
      }
         
   }
   token.type = eCRLTT_Number;
   token.line = line;
   token.col = col;
   token.text = StringBuffer_StringCopy(buf);
   CRL_AddToken(lexer, &token);
}

static void CRL_LexString(struct CRLexer * lexer, 
                          struct InputSys * is, 
                          struct StringBuffer * buf)
{
   int c;
   int term;
   struct CRLToken token;
   
   // Assume next token is either ' or "
   term = InputSys_Next(is, &token.line, &token.col);
   while((c = InputSys_Next(is, NULL, NULL)) >= 0)
   {
      if(c == '\\')
      {
         c = InputSys_Next(is, NULL, NULL);
         switch(c)
         {
         case 'n':
            StringBuffer_Add(buf, '\n');
            break;
         case 'r':
            StringBuffer_Add(buf, '\r');
            break;
         case 't':
            StringBuffer_Add(buf, '\t');
            break;
         case '\\':
            StringBuffer_Add(buf, '\\');
            break;
         case '0':
            StringBuffer_Add(buf, '\0'); // Maybe this is a bad Idea
            break;
         case '\'':
            StringBuffer_Add(buf, '\'');
            break;
         case '"':
            StringBuffer_Add(buf, '"');
            break;
         default:
            StringBuffer_Add(buf, '\\');
            StringBuffer_Add(buf, c);
            break;
         }
      }
      else if(c == term)
      {
         break;
      }
      else
      {
         StringBuffer_Add(buf, c);
      }
   }
   if(term == '"')
   {
      token.type = eCRLTT_String;
   }
   else
   {
      token.type = eCRLTT_Character;
   }
   token.text = StringBuffer_StringCopy(buf);
   CRL_AddToken(lexer, &token);
}

static void CRL_LexSymbolOrLiteral(struct CRLexer * lexer, 
                                   struct InputSys * is, 
                                   struct StringBuffer * buf)
{
   int c;
   int textFlag;
   int foundIndex = 0;
   enum CRLTokenType type;
   struct CRLToken token;

   // We must take at least the first characater
   c = InputSys_Next(is, &token.line, &token.col);
   StringBuffer_Add(buf, c);
   textFlag = IsText(c);
   if(!textFlag)
   {
      foundIndex = CRL_SymbolLookupPartial(buf, 0);
      if(foundIndex < 0)
      {
         token.type = eCRLTT_Unknown;
         token.text = StringBuffer_StringCopy(buf);
         CRL_AddToken(lexer, &token);
         return;
      }
   }
   
   
   while((c = InputSys_Peek(is, NULL, NULL)) >= 0)
   {
      if(IsWhitespace(c) || 
        (textFlag && !IsText(c) && !IsDigit(c)) || 
        (!textFlag && (IsText(c) || IsDigit(c))))
      {
         break;
      }
      else
      {
         StringBuffer_Add(buf, c);
         if(textFlag)
         {
            (void)InputSys_Next(is, NULL, NULL);
         }
         else
         {
            int prevFoundIndex = foundIndex;
            // Greedy Matching of operators
            foundIndex = CRL_SymbolLookupPartial(buf, foundIndex);
            if(foundIndex < 0)
            {
               StringBuffer_UndoAdd(buf);
               foundIndex = prevFoundIndex;
               break;
            }
            else
            {
               (void)InputSys_Next(is, NULL, NULL);
            }
         }
      }
   }
   
   if(!textFlag)
   {
      if(StringBuffer_CheckMatch(buf, TokenTextPairs[foundIndex].text) == 1)
      {
         token.type = TokenTextPairs[foundIndex].type;
      }
      else
      {
         token.type = eCRLTT_Unknown;
      }            
   }   
   
   token.text = StringBuffer_StringCopy(buf);
   if(textFlag)
   {
      type = CRL_SymbolLookup(buf);
      if(type == eCRLTT_Unknown)
      {
         token.type = eCRLTT_Literal;
      }
      else
      {
         token.type = type;
      }
   }
   CRL_AddToken(lexer, &token);

}

static void CRL_Lex(struct CRLexer * lexer, struct InputSys * is)
{
   int c = 0;

   struct StringBuffer buf;
   int line, col;
   
   StringBuffer_Init(&buf);
   while((c = InputSys_Peek(is, &line, &col)) >= 0)
   {
      StringBuffer_Clear(&buf);
      if(IsWhitespace(c))
      {
         CRL_IgnoreToNonWhitespace(is);
      }
      else if(c == '#')
      {
         CRL_IgnoreToEndOfMacro(is);
      }
      else if(c == '/')
      {
         CRL_LexSlash(lexer, is, &buf);
      }
      else if(IsDigit(c))
      {
         CRL_LexNumber(lexer, is, &buf, line, col);
      }
      else if(c == '"' || c == '\'')
      {
         CRL_LexString(lexer, is, &buf);
      }
      else
      {
         CRL_LexSymbolOrLiteral(lexer, is, &buf);
      }
   }
   StringBuffer_Destory(&buf);
}

static void CRL_LexFile(struct CRLexer * lexer, FILE * fh)
{
   struct InputSys is;
   InputSys_InitFromFile(&is, fh);
   CRL_Lex(lexer, &is);
   InputSys_Destroy(&is);
}

static void CRL_LexFileName(struct CRLexer * lexer, const char * filename)
{
   FILE * fh;
   fh = fopen(filename, "r");
   
   if(fh != NULL)
   {
      CRL_LexFile(lexer, fh);
      fclose(fh);
   }
   
}

struct CRLexer * CRLexer_New(const char * filename)
{
   struct CRLexer * lexer;
   lexer = malloc(sizeof(struct CRLexer));
   
   CRL_InitTokens(lexer);
   CRL_LexFileName(lexer, filename);
   return lexer;
}

void CRLexer_Destroy(struct CRLexer * lexer)
{
   CRL_FreeTokens(lexer);
   free(lexer);
}

struct CRLToken * CRLexer_GetTokens(struct CRLexer * lexer, size_t * count)
{
   if(count != NULL)
   {
      (*count) = lexer->count;
   }
   return lexer->tokens;
}
