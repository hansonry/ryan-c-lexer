#ifndef __RCLexer_H__
#define __RCLexer_H__

struct RCLexer;

enum RCLTokenType
{
   eRCLTT_Unknown,
   eRCLTT_Literal,
   eRCLTT_NumberInteger,
   eRCLTT_NumberHexPrefix, // 0x
   eRCLTT_NumberHex,
   eRCLTT_NumberOctPrefix, // 0
   eRCLTT_NumberOct,
   eRCLTT_NumberIntegerSuffixLong, // l
   eRCLTT_NumberIntegerSuffixLongLong, // ll
   eRCLTT_NumberIntegerSuffixUnsigned, // u
   eRCLTT_NumberHexFraction, // 0x(AA.BB) or 0x(0.FF)
   eRCLTT_NumberFraction, // (0.345) or (.435) or (24.34) or (2343)e+3
   eRCLTT_NumberExponent, // e 
   eRCLTT_NumberBinaryExponent, // p
   eRCLTT_NumberExponentValue, // 1e(+1) or 0x34p(-3)
   eRCLTT_NumberFractionSuffixLongDouble, // l
   eRCLTT_NumberFractionSuffixFloat, // f
   eRCLTT_String,
   eRCLTT_Character,
   eRCLTT_Plus,
   eRCLTT_Minus,
   eRCLTT_Asterisk,
   eRCLTT_Divide,
   eRCLTT_PlusEquals,
   eRCLTT_MinusEquals,
   eRCLTT_DivideEquals,
   eRCLTT_MultiplyEquals,
   eRCLTT_Increment,
   eRCLTT_Decrement,
   eRCLTT_Comma,
   eRCLTT_Dot,
   eRCLTT_DereferenceDot,
   eRCLTT_Colon,
   eRCLTT_OpenParen,
   eRCLTT_ClosedParen,
   eRCLTT_OpenSquareBraket,
   eRCLTT_ClosedSquareBraket,
   eRCLTT_OpenCurlyBracket,
   eRCLTT_ClosedCurlyBracket,
   eRCLTT_Semicolon,
   eRCLTT_Equals,
   eRCLTT_DoubleEquals,
   eRCLTT_LessThan,
   eRCLTT_GreaterThan,
   eRCLTT_LessThanOrEqualTo,
   eRCLTT_GreaterThanOrEqualTo,
   eRCLTT_NotEqual,
   eRCLTT_BitShiftLeft,
   eRCLTT_BitShiftRight,
   eRCLTT_BitwiseNot,
   eRCLTT_BitwiseAnd,
   eRCLTT_BitwiseOr,
   eRCLTT_LogicalNot,
   eRCLTT_LogicalAnd,
   eRCLTT_LogicalOr,
   eRCLTT_Struct,
   eRCLTT_Enum,
   eRCLTT_Union,
   eRCLTT_Typedef,
   eRCLTT_If,
   eRCLTT_Else,
   eRCLTT_For,
   eRCLTT_Do,
   eRCLTT_While,
   eRCLTT_Goto,
   eRCLTT_Switch,
   eRCLTT_Case,
   eRCLTT_Break,
   eRCLTT_Default,
   eRCLTT_Static,
   eRCLTT_Extern,
   eRCLTT_Inline,
   eRCLTT_Volatile,
   eRCLTT_Void,
   eRCLTT_Const,
   eRCLTT_Return,
   eRCLTT_Auto,
   eRCLTT_PP_Define,
   eRCLTT_PP_Include,
   eRCLTT_PP_AngleString,
   eRCLTT_PP_Ifdef,
   eRCLTT_PP_Ifndef,
   eRCLTT_PP_Else,
   eRCLTT_PP_Endif,
   eRCLTT_PP_If,
   eRCLTT_PP_Elif,
   eRCLTT_PP_Defined,
   eRCLTT_PP_Error,
   eRCLTT_PP_Warning,
   eRCLTT_PP_Pragma,
   eRCLTT_PP_Undef,
};

struct RCLToken
{
   const char * text;
   enum RCLTokenType type;
   int preprocessorFlag; // 1 = Inside Preprocessor directive, 0 = Normal C code
   int line;
   int col;
   const char * filename;
};

struct RCLexer * RCLexer_MakeFromFilename(const char * filename);
void RCLexer_Free(struct RCLexer * lexer);

int RCLexer_GetTokens(struct RCLexer * lexer, struct RCLToken * tokens, int count);


#endif // __RCLexer_H__
